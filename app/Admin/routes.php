<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    $router->resource('/input', InputController::class);
    $router->resource('/tobereceived', TobeReceivedController::class);
    $router->resource('/newtobereceived', NewTobeReceivedController::class);
    $router->resource('/receiving', ReceivingController::class);
    $router->resource('/received', ReceivedController::class);
    $router->resource('/drivers', DriversController::class);
    $router->resource('/confirm', ConfirmController::class);

});
