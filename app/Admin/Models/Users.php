<?php

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    public $table = 'users_tb';
    public $primaryKey = 'USERID';
}