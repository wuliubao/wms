<?php

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class Drivers extends Model
{
    public $table = 'drivers_tb';
    public $primaryKey = 'DRIVERID';
}