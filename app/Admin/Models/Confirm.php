<?php

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class Confirm extends Model
{
    public $table = 'confirm_tb';
    public $primaryKey = 'DRIVERID';

    public function Business()
	{
	    return $this->hasOne('App\Admin\Models\Business','BUSINESSID','BUSINESSID');
	}
}
