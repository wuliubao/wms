<?php

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class Transport extends Model
{
    public $table = 'transport_tb';
    public $primaryKey = 'BUSINESSID';

    public function Business()
	{
	    return $this->hasOne('App\Admin\Models\Business','BUSINESSID','BUSINESSID');
	}

    public function Drivers()
	{
	    return $this->hasOne('App\Admin\Models\Drivers','DRIVERID','DRIVERID');
	}
}