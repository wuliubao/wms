<?php

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    public $table = 'business_tb';
    public $primaryKey = 'BUSINESSID';
}