<?php

namespace App\Admin\Controllers;

use App\Admin\Models\Transport;
use App\Admin\Models\Business;
use App\Admin\Models\Drivers;
use App\Admin\Models\Confirm;


use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class TobeReceivedController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public $editId ;
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('待排接货单');
            $content->description('description');
            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');
            $this->editId = $id;
            $content->body($this->form());
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Business::class, function (Grid $grid) {
            $grid->column('WAYBILLNUM',"运单号")->display(function ($title) {
                return "<span style='color:white;background-color: #00a65a;padding: 3px;border-radius: 3px;font-weight: bolder'>$title</span>";
            });
            $grid->OWNER('货主');
            $grid->RESERVEDTIME('预约时间');
            $grid->BOOKEDBAG('数量（包）');
            $grid->BOOKEDBUNCH('数量（匹）');
            $grid->GOODSSIZE('体积');
            $grid->DESTINATION('目的地');
            $grid->OWNERTEL('联系电话');
            $grid->RECEIVEADD('接货地址');
//            $grid->__actions__("安排发货")->display(function ($title) {
//                return "<span style='color:white;background-color: #00a65a;padding: 3px;border-radius: 3px;font-weight: bolder'>$title</span>";
//            });;

            $grid->actions(function ($actions) {

                $actions->disableDelete();

            });

            //订单状态：1、已接待
            $grid->model()->where('BUSINESSSTATUS', '=', 1);
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Transport::class, function (Form $form) {
            $liudata = Business::where('BUSINESSID',$this->editId)->first();
            if ($liudata) {
                $yundan =$liudata->WAYBILLNUM;
                $OWNER =$liudata->OWNER;
                $form->display('Business.WAYBILLNUM', '运单号')->default($yundan)->with(function () use($yundan)  {
                    return "<span style='color:#bb260d;font-weight: bolder'>$yundan</span>";
                });
                $form->display('Business.OWNER', '货主')->default($OWNER)->with(function () use($OWNER)  {
                    return "<span style='color:#bb260d;font-weight: bolder'>$OWNER</span>";
                });
            }


            $directors = [];
            $users = Drivers::all();
            foreach ($users as $flight) {
//                array_push($directors,$flight->DRIVERNAME);
                $directors[$flight->DRIVERID] = $flight->DRIVERNAME;
            }
            $form->select('DRIVERID', '请选择司机')->default('')->options($directors);
            $form->time('RESERVEDTIME', '司机出发时间')->format('YYYY-MM-DD HH:mm:ss');

//            $form->setAction('edit');
            $form->setAction('../../tobereceived');
            $form->saving(function (Form $form) {

                return redirect('/admin/receiving');
                // 抛出异常
                throw new \Exception('出错啦。。。');

            });
        });
    }
}
