<?php

namespace App\Admin\Controllers;

use App\Admin\Models\Business;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class InputController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('接线员录入');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Business::class, function (Grid $grid) {

            $grid->BUSINESSID('ID')->sortable();

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Business::class, function (Form $form) {


            $states = [
                'on'  => ['value' => 2, 'text' => '安排', 'color' => 'success'],
                'off' => ['value' => 1, 'text' => '不安排', 'color' => 'danger'],
            ];

            $form->text('OWNER', '货主');
//            $form->display('OWNER', '货主');
            $form->number('BOOKEDBAG', '数量（包）');
            $form->number('BOOKEDBUNCH', '数量（匹）');

            // 添加text类型的input框
            $form->text('RECEIVEADD', '接货地址');
            $form->text('DESTINATION', '目的地');
            $form->text('WAYBILLNUM', '运单号码');
            $form->textarea('GOODSSIZE', '大小描述');
            $form->time('RESERVEDTIME', '预约时间')->format('YYYY-MM-DD HH:mm:ss');
            $form->mobile('OWNERTEL', '联系电话')->options(['mask' => '999 9999 9999']);
            $form->switch('BUSINESSSTATUS', '是否安排出货')->default('off')->states($states);
            $form->setAction('input');

            $form->saved(function (Form $form) {
                if($form->BUSINESSSTATUS === "on") {
                    return redirect('/admin/tobereceived/1/edit');
                } else {
                    return redirect('/admin/tobereceived');
                };
            });
        });
    }
}
