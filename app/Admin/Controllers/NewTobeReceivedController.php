<?php

namespace App\Admin\Controllers;

use App\Admin\Models\Transport;
use App\Admin\Models\Business;
use App\Admin\Models\Drivers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class NewTobeReceivedController extends Controller
{
    use ModelForm;

    public $editId;
    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('待排接货单');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');
            $this->editId = $id;
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Transport::class, function (Grid $grid) {

            $grid->column('Business.WAYBILLNUM',"运单号")->display(function ($title) {
                return "<span style='color:white;background-color: #00a65a;padding: 3px;border-radius: 3px;font-weight: bolder'>$title</span>";
            });
            $grid->column('Business.OWNER','货主');
            $grid->column('Business.RESERVEDTIME','预约时间');
            $grid->column('Business.BOOKEDBAG','数量（包）');
            $grid->column('Business.BOOKEDBUNCH','数量（匹）');
            $grid->column('Business.GOODSSIZE','体积');
            $grid->column('Business.DESTINATION','目的地');
            $grid->column('Business.OWNERTEL','联系电话');
            $grid->column('Business.RECEIVEADD','接货地址');

//            $grid->__actions__("安排发货")->display(function ($title) {
//                return "<span style='color:white;background-color: #00a65a;padding: 3px;border-radius: 3px;font-weight: bolder'>$title</span>";
//            });;

            $grid->actions(function ($actions) {

                $actions->disableDelete();

            });

            //订单状态：1、已接待
//            $grid->model()->whereHas('Business.BUSINESSSTATUS', '=', 1);
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Transport::class, function (Form $form) {
//            $data = Business::where('BUSINESSID',$this->editId)->first();
//            $WAYBILLNUM =$data->WAYBILLNUM;
//            $OWNER =$data->OWNER;
//            $form->display('WAYBILLNUM', '运单号')->default($WAYBILLNUM)->with(function () use($WAYBILLNUM)  {
//                return "<span style='color:#bb260d;font-weight: bolder'>$WAYBILLNUM</span>";
//            });
//            $form->display('OWNER', '货主')->default($OWNER)->with(function () use($OWNER)  {
//                return "<span style='color:#bb260d;font-weight: bolder'>$OWNER</span>";
//            });



//            $form->text('BUSINESSID','ID')->default($this->editId);
            $directors = [];
            $users = Drivers::all();
            foreach ($users as $flight) {
//                array_push($directors,$flight->DRIVERNAME);
                $directors[$flight->DRIVERID] = $flight->DRIVERNAME;
            }
            $form->select('DRIVERID', '请选择司机')->default('')->options($directors);
            $form->time('RESERVEDTIME', '司机出发时间')->format('YYYY-MM-DD HH:mm:ss');

        });
    }
}
