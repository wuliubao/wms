<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Content;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;

use App\Admin\Models\Business;

class InputController extends Controller
{
    use ModelForm;
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('接线员录入');
            $form = Admin::form(Business::class, function(Form $form){

                // 显示记录id
                $form->text('OWNER', '货主');
                $form->number('REALBAG', '数量（包）');
                $form->number('REALBUNCH', '数量（匹）');

                // 添加text类型的input框
                $form->text('RECEIVEADD', '接货地址');
                $form->text('DESTINATION', '目的地');
                $form->text('WAYBILLNUM', '运单号码');
                $form->textarea('GOODSSIZE', '大小描述');
                $form->text('RESERVEDTIME', '预约时间');
                $form->text('OWNERTEL', '联系电话');



//                $directors = [
//                    'John'  => "2017-09-10",
//                    'Smith' => 2,
//                    'Kate'  => 3,
//                ];
//
//                $form->select('director', '预约时间')->options($directors);
//
//                // 添加describe的textarea输入框
//                $form->textarea('describe', '大小简介');

                // 数字输入框

                // 添加开关操作
                $form->switch('released', '确认信息？');



            });
//
            $content->row($form);
        });
    }
}
