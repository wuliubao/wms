<?php

namespace App\Admin\Controllers;

use App\Admin\Models\Transport;
use App\Admin\Models\Business;
use App\Admin\Models\Confirm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class ReceivedController extends Controller
{
    use ModelForm;

    public $editId ;
    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('已拍货清单');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');
            $this->editId = $id;
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Transport::class, function (Grid $grid) {
            
            $grid->column('Business.WAYBILLNUM','运单号');
            $grid->column('Business.OWNER','货主');
            $grid->column('Drivers.LICENSEPLATE','接货车辆');
            $grid->column('Business.BUSINESSTIME','接单时间');
            $grid->column('Business.BOOKEDBAG','预订数量（包）');
            $grid->column('Business.BOOKEDBUNCH','预订数量（匹）');
            $grid->REALBAG('实际数量（包）');
            $grid->REALBUNCH('实际数量（匹）');
            $grid->column('Drivers.TEL','收货人号码');
            $grid->ARRIVETIME('收货时间');
            $grid->column('Business.DESTINATION','目的地');
            $grid->actions(function ($actions) {

                $actions->disableDelete();

            });

            //订单状态：3、已收货
            $grid->model()->where('BUSINESSSTATUS', '=', 3);
        });
    }

    protected function sendMessage() {
        $ch = curl_init();

        // 必要参数
        $apikey = "5b63116278928d2b2ccf9c93b7f8dd62"; //修改为您的apikey(https://www.yunpian.com)登录官网后获取
        $mobile = "15967154407"; //请用手机号代替
        $text="【一茶科技】您的订单编号：#200000#,物流信息：#300042340#";

        // 发送短信
        $data=array('text'=>$text,'apikey'=>$apikey,'mobile'=>$mobile);
        curl_setopt ($ch, CURLOPT_URL, 'https://sms.yunpian.com/v2/sms/single_send.json');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        $json_data = curl_exec($ch);

        //如果curl发生错误，打印出错误
        if(curl_error($ch) != ""){
            echo 'Curl error: ' .curl_error($ch);
        }
        //解析返回结果（json格式字符串）
        $array = json_decode($json_data,true);
        dd($array);

    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Transport::class, function (Form $form) {
            $data = Business::where('BUSINESSID',$this->editId)->first();
            if ($data) {
                $WAYBILLNUM =$data->WAYBILLNUM;
                $OWNER =$data->OWNER;
                $form->display('WAYBILLNUM', '运单号')->default($WAYBILLNUM)->with(function () use($WAYBILLNUM)  {
                    return "<span style='color:#bb260d;font-weight: bolder'>$WAYBILLNUM</span>";
                });
                $form->display('OWNER', '货主')->default($OWNER)->with(function () use($OWNER)  {
                    return "<span style='color:#bb260d;font-weight: bolder'>$OWNER</span>";
                });
            }



            $form->number('REALBAG', '总金额')->default("1000");
            $form->saving(function (Form $form) {
//                $data = Business::where('BUSINESSID',$this->editId)->first();
//                if ($data) {
//                    $WAYBILLNUM =$data->WAYBILLNUM;
//                    dd($WAYBILLNUM);
//                    $this->sendMessage($WAYBILLNUM);
//                }
                $this->sendMessage();
                return redirect('/admin/confirm');
                // 抛出异常
                throw new \Exception('出错啦。。。');

            });

        });
    }
}
