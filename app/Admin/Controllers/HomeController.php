<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Grid;
use Encore\Admin\Form;
use Encore\Admin\Widgets\InfoBox;

class HomeController extends Controller
{
    public function index()
    {
        return Admin::content(function (Content $content) {

//            $grid = Admin::grid(Post::class, function(Grid $grid){
//
//                // 第一列显示id字段，并将这一列设置为可排序列
//                $grid->BUSINESSID('ID号');
//                $grid->WAYBILLNUM('订单编号');
//                $grid->DESTINATION('发出地点');
//                $grid->BOOKEDBAG('订单数量');
//                $grid->RECEIVEADD('到达地点');
//                $grid->RESERVEDTIME('到达时间');
//                $grid->BUSINESSTIME('接货时间');
//            });

//            $grid = Admin::grid(Post::class, function(Grid $grid){
//
//                // 第一列显示id字段，并将这一列设置为可排序列
//                $grid->DRIVERID('ID号');
//                $grid->DRIVERNAME('司机姓名');
//                $grid->LICENSEPLATE('车牌号');
////                $grid->TEL('联系电话');
//                $grid->column('TEL')->display(function ($title) {
//
//                    return "<span style='border-radius:2px;background-color:#008d4c;color:#ffffff;'>$title</span>";
//
//                });
//
//            });
            $content->header('数据统计页面');
            $content->description('待发货、已收货、核价单数据统计.');


            $content->body(view('admin.title'));
            $content->row(function(Row $row) {
                $row->column(2, new InfoBox('司机人数', 'users', 'aqua', '/admin/users', '20'));
                $row->column(2, new InfoBox('核价单', 'file', 'yellow', '/admin/users', '500'));
                $row->column(2, new InfoBox('待排货清单', 'file', 'red', '/admin/users', '30'));
                $row->column(2, new InfoBox('已收货清单', 'file', 'green', '/admin/users', '50'));
            });

            $content->body(view('admin.title1'));
            $content->body(view('admin.chars.bar'));

//            $content->row($grid);



//            $content->row(function (Row $row) {
//                $form = Admin::form(Post::class, function(Form $form){
//
//                    // 显示记录id
//                    $form->display('id', '货主');
//
//                    // 添加text类型的input框
//                    $form->text('title', '接货地址');
//                    $form->text('title', '目的地');
//
//                    $directors = [
//                        'John'  => "2017-09-10",
//                        'Smith' => 2,
//                        'Kate'  => 3,
//                    ];
//
//                    $form->select('director', '预约时间')->options($directors);
//
//                    // 添加describe的textarea输入框
//                    $form->textarea('describe', '大小简介');
//
//                    // 数字输入框
//                    $form->number('rate', '数量');
//
//                    // 添加开关操作
//                    $form->switch('released', '确认信息？');
//
//                    // 添加日期时间选择框
//                    $form->dateTime('release_at', '发布时间');
//
//                    // 两个时间显示
//                    $form->display('created_at', '创建时间');
//                    $form->display('updated_at', '修改时间');
//                });
//                $row->column(1,"");
//                $row->column(10,$form);
//                $row->column(1,"");
//
//            });

//            $content->row(function (Row $row) {
//
//                $row->column(4, function (Column $column) {
//                    $column->append(Dashboard::environment());
//                });
//
//                $row->column(4, function (Column $column) {
//                    $column->append(Dashboard::extensions());
//                });
//
//                $row->column(4, function (Column $column) {
//                    $column->append(Dashboard::dependencies());
//                });
//            });
        });
    }
}
