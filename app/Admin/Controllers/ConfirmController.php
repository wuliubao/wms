<?php

namespace App\Admin\Controllers;

use App\Admin\Models\Confirm;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class ConfirmController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('核价单');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Confirm::class, function (Grid $grid) {

            $grid->WAYBILLNUM("运单号码");
            $grid->column('Business.OWNER','货主');
            $grid->MONEY("金额");
            $grid->REMARKS("备注");
            $grid->actions(function ($actions) {

                $actions->disableDelete();
                $actions->disableEdit();

            });

            $grid->disableActions();
            $grid->disableRowSelector();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Confirm::class, function (Form $form) {

            $form->text('DRIVERID', '业务ID');

            $form->text('WAYBILLNUM', '运单号码');
            $form->text('MONEY', '金额');
            $form->text('REMARKS', '金额');
            $form->ignore(['updated_at', 'created_at']);
            $form->setAction('confirm');
        });
    }
}
