<?php

namespace App\Admin\Controllers;

use App\Admin\Models\Business;
use App\Admin\Models\Transport;
use App\Admin\Models\Drivers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class ReceivingController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public $editId ;
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('排货中清单');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');
            $this->editId = $id;
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Transport::class, function (Grid $grid) {

            $grid->column('Business.WAYBILLNUM','运单号');
            $grid->column('Business.OWNER','货主');
            $grid->column('Drivers.LICENSEPLATE','接货车辆');
            $grid->column('Business.RESERVEDTIME','预约时间');

            $grid->column('Business.BOOKEDBAG','预订数量（包）');
            $grid->column('Business.BOOKEDBUNCH','预订数量（匹）');

            $grid->actions(function ($actions) {

                $actions->disableDelete();

            });
            //订单状态：3、已收货
            $grid->model()->where('BUSINESSSTATUS', '=', 2);
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Transport::class, function (Form $form) {

            $data = Business::where('BUSINESSID',$this->editId)->first();
            if ($data) {
                $WAYBILLNUM =$data->WAYBILLNUM;
                $OWNER =$data->OWNER;
                $form->display('WAYBILLNUM', '运单号')->default($WAYBILLNUM)->with(function () use($WAYBILLNUM)  {
                    return "<span style='color:#bb260d;font-weight: bolder'>$WAYBILLNUM</span>";
                });
                $form->display('OWNER', '货主')->default($OWNER)->with(function () use($OWNER)  {
                    return "<span style='color:#bb260d;font-weight: bolder'>$OWNER</span>";
                });
            }



            $form->time('ARRIVETIME', '收货时间')->format('YYYY-MM-DD HH:mm:ss');
            $form->number('REALBAG', '实际收货数量（包）');
            $form->number('REALBUNCH', '实际收货数量（匹）');
//            $form->setAction('../../received');
//            $form->saved(function (Form $form) {
//                return redirect('/admin/received');
//            });

            $form->saving(function (Form $form) {

                return redirect('/admin/received');
                // 抛出异常
                throw new \Exception('出错啦。。。');

            });
        });
    }
}
