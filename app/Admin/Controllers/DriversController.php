<?php

namespace App\Admin\Controllers;

use App\Admin\Models\Drivers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class DriversController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('司机信息');
            $content->description('description');

            $content->body($this->grid());
            $content->body($this->form());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Drivers::class, function (Grid $grid) {

            $grid->DRIVERNAME('司机姓名')->sortable();

            $grid->LICENSEPLATE("车牌号");
            $grid->TEL("联系号码");
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Drivers::class, function (Form $form) {

            $form->text('DRIVERNAME', '司机姓名');

            $form->text('LICENSEPLATE', '车牌号');
            $form->text('TEL', '联系号码');
        });
    }
}
