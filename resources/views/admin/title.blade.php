<style>
    .title {
        font-size: 30px;
        letter-spacing:5px;
        color: #636b6f;
        font-family: '微软雅黑', sans-serif;
        font-weight: 100;
        display: block;
        text-align: center;
        margin: 50px 0 10px 0px;
    }

    .links {
        text-align: center;
        margin-bottom: 20px;
    }

    .links > a {
        color: #636b6f;
        padding: 0 25px;
        font-size: 12px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
    }
</style>

<div class="title">
    各项数据统计
</div>
