

## 安装配置

系统使用laravel框架 使用xampp来配置服务器环境 

- 安装[XAMPP](https://www.apachefriends.org/index.html)注意版本
- 将工程放在xampp安装目录的htdocs目录下
- 更新composer依赖
- 更改Apache的httpd.conf文件中的DocumentRoot和Directory变量，前面安装目录不变，后面添加:目录/public
- 导入test.sql到数据库中，可使用本地 phpmyadmin (http://localhost/phpmyadmin)
- 输入http://localhost/admin 既可访问

## 相关页面
包含常用页面:登录、统计、显示、操作、增删改查、确认页面

-  登录页面
  ![](readme/login.jpg)
  
-  统计页面
  ![](readme/info_stat.png)

-  数据展示
  ![](readme/info_show.png)

